package com.codenjoy.dojo.snake.client;

/*-
 * #%L
 * Codenjoy - it's a dojo-like platform from developers to developers.
 * %%
 * Copyright (C) 2018 Codenjoy
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.codenjoy.dojo.client.Solver;
import com.codenjoy.dojo.client.WebSocketRunner;
import com.codenjoy.dojo.services.*;

import java.util.*;

/**
 * User: your name
 */
public class YourSolver implements Solver<Board> {

    private Dice dice;
    private Board board;

    public YourSolver(Dice dice) {
        this.dice = dice;
    }

    @Override
    public String get(Board board) {
        this.board = board;

        Point head = board.getHead();
        Point stone = board.getStones().get(0);
        Point apple = board.getApples().get(0);

        if (head == null){
            return Direction.UP.toString();
        }

        List<Point> barriers = board.getBarriers();
        Optional<List<Point>> trace;
        if (board.getSnake().size() == 50){

            barriers.remove(stone);
            trace = new Lee(board.size(), board.size()).trace(head, stone, barriers);
        }else {

            trace = new Lee(board.size(), board.size()).trace(head, apple, barriers);
        }

        //System.out.println("Trace " + trace);
        Point newStep;

        if (trace.isPresent()){
            newStep = trace.get().get(0);
        }else {
            barriers.remove(stone);
            trace = new Lee(board.size(), board.size()).trace(head, stone, barriers);
            if (trace.isPresent()){
                newStep = trace.get().get(0);
            }else {
                System.out.println("Max snake " + board.getSnake().size());
                newStep = new Lee(board.size(), board.size()).getNextPoint(head, barriers);
            }

        }


        if (newStep.getX() < head.getX()) return Direction.LEFT.toString();
        if (newStep.getX() > head.getX()) return Direction.RIGHT.toString();
        if (newStep.getY() < head.getY()) return Direction.DOWN.toString();
        if (newStep.getY() < head.getY()) return Direction.UP.toString();


        return Direction.UP.toString();
    }

    public static void main(String[] args) {
        //http://64.226.126.93/codenjoy-contest/board/player/wetlmk70cy1d342y3ay8?code=1113310469429233837
        WebSocketRunner.runClient(
                // paste here board page url from browser after registration
                //"http://codenjoy.com:80/codenjoy-contest/board/player/3edq63tw0bq4w4iem7nb?code=1234567890123456789",
                "http://64.226.126.93/codenjoy-contest/board/player/wetlmk70cy1d342y3ay8?code=1113310469429233837",
                new YourSolver(new RandomDice()),
                new Board());
    }

}
