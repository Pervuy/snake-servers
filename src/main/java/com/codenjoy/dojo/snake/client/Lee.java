package com.codenjoy.dojo.snake.client;

import com.codenjoy.dojo.services.Direction;
import com.codenjoy.dojo.services.Point;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Lee {

    private final int EMPTY = 0;
    private final int START = 1;
    private final int width;
    private final int height;
    private final int[][] board;

    public Lee(int width, int height) {
        this.width = width;
        this.height = height;
        this.board = new int[height][width];
    }

    private int get(int x, int y) {
        return board[y][x];
    }

    private void set(int x, int y, int value) {
        board[y][x] = value;
    }

    private int get(Point point) {
        return get(point.getX(), point.getY());
    }

    private void set(Point point, int value) {
        set(point.getX(), point.getY(), value);
    }

    private boolean isUnvisited(Point point) {
        return get(point) == EMPTY;
    }

    private Supplier<Stream<Direction>> deltas() {

        return ()->Direction.getValues().stream();
    }

    private Stream<Point> neighbours(Point point, List<Point> obstacles) {
        return deltas().get()
                .map(d -> d.change(point))
                .filter(p -> !obstacles.contains(p));
    }

    private Stream<Point> neighboursUnvisited(Point point, List<Point> obstacles) {
        return neighbours(point, obstacles)
                .filter(this::isUnvisited);
    }

    private List<Point> neighboursByValue(Point point, int value, List<Point> obstacles) {
        return neighbours(point, obstacles)
                .filter(p -> get(p) == value)
                .toList();
    }

    public Point getNextPoint(Point start,List<Point> obstacles){
        List<Point> list = neighbours(start, obstacles).toList();
        if (list.isEmpty()){
            return start;
        }else {
            return list.get(0);
        }

    }
    public Optional<List<Point>> trace(Point start, Point finish, List<Point> obstacles) {
        // 1. initialization

        int[] counter = {START};
        set(start, counter[0]);
        counter[0]++;
        boolean found = false;
        // 2. fill the board
        for (Set<Point> curr = new HashSet<>(Set.of(start)); !(found || curr.isEmpty()); counter[0]++) {
            Set<Point> next = curr.stream()
                    .flatMap(p->neighboursUnvisited(p,obstacles))
                    .collect(Collectors.toSet());

            next.forEach(p -> set(p, counter[0]));
            found = next.contains(finish);
            curr = next;
        }
        // 3. backtrace (reconstruct the path)
        if (!found) return Optional.empty();
        LinkedList<Point> path = new LinkedList<>();
        path.add(finish);
        counter[0]--;

        Point curr = finish;
        while (counter[0]-1 > START) {
            counter[0]--;
            Point prev = neighboursByValue(curr, counter[0],obstacles).get(0);
            if (prev == start) break;
            path.addFirst(prev);
            curr = prev;
        }
        return Optional.of(path);
    }

}
